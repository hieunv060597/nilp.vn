(function ($) {

    window.onload = function () {
        

        $(document).ready(function () {
      
			open_search_header();
			carosel_s1();
			stick_header_bottom();
			accordion_mobile_header();			
			back_to_top();
			push_right_header_mb();
			suggest_box();
        });

        
        
    };

    
 
})(jQuery);

function open_search_header() {
    var overlay_search = document.querySelector('#header #overlay-search');
    var button_search = document.querySelector('#header .header-bottom .nav-bar-main .ul-nav-bar .search-header button');
    var header_search_box = document.querySelector('#header #search-header-box');

    button_search.onclick = function () {
        overlay_search.classList.add('show_search');
        header_search_box.classList.add('show-search-hd-ct');
    }

    overlay_search.onclick = function () {
        overlay_search.classList.remove('show_search');
        header_search_box.classList.remove('show-search-hd-ct');
    }
}

function carosel_s1() {
	$('#wrapper #home-page .first-news .slider-img-content .owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		responsiveClass: true,
		nav: true,
		navText: ["<div class='nav-btn prev-slide'><i class = 'fa fa-chevron-left'></i></div>", "<div class='nav-btn next-slide'><i class = 'fa fa-chevron-right'></i></div>"],
		dots: false,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		
		responsive: {

			0: {
				items: 1,
				
			},

			578: {
				items: 1,
				
			},

			768: {
				items: 1,
				
			},

			1000: {
				items: 1,
				
			},

			1200: {
				items: 1,
				
			},

			1400: {
				items: 1,
				
			}
		}
	})
}

function stick_header_bottom(){
	var check = true;
	var header_main = document.querySelector('#header .header-main');
	var header_bottom = document.querySelector('#header .header-bottom');

	

	window.addEventListener('scroll', function () {
		if (window.pageYOffset > 220) {
			if (check == true) {
				header_main.classList.add('off-header');
				header_bottom.classList.add('fixed-stuck');
				check = false;
			}
		}

		else {
			if (check == false) {
				header_main.classList.remove('off-header');
				header_bottom.classList.remove('fixed-stuck');
				check = true;
            }
        }
    })
}

function accordion_mobile_header() {
	var acc = document.querySelectorAll('.list-moblibe-item');
	var i;

	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function () {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.maxHeight) {
				panel.style.padding = "0px 5px";
				panel.style.maxHeight = null;
				
			} else {
				panel.style.padding = "10px 5px";
				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		});
	}
}

function push_right_header_mb() {
	var button_open_nav_mb = document.querySelector('#btn-bar');
	var menu_mb = document.querySelector('#header .header-moblie .site-nav-moblie');
	var box_push = document.querySelector('#main');
	var background = document.querySelector('#wrapper #main .overlay-main');
	var btn_close = document.querySelector('#header .header-moblie .site-nav-moblie .bar-scl-box .bar-scl');
	button_open_nav_mb.onclick = function () {
		menu_mb.classList.add('push-site-mb');
		box_push.classList.add('push-wrapper');
		background.classList.add('show');
	}

	background.onclick = function () {
		menu_mb.classList.remove('push-site-mb');
		box_push.classList.remove('push-wrapper');
		background.classList.remove('show');
	}

	btn_close.onclick = function () {
		menu_mb.classList.remove('push-site-mb');
		box_push.classList.remove('push-wrapper');
		background.classList.remove('show');
	}
}

function suggest_box() {
	var hide_btn = document.querySelector('#wrapper #main #post-detail-blog .main-post .content-post .content-box .suggestions-box .title-suggest .hide-and-show .hide');
	var content = document.querySelector('#wrapper #main #post-detail-blog .main-post .content-post .content-box .suggestions-box .suggestions-content');
	var box = document.querySelector('#wrapper #main #post-detail-blog .main-post .content-post .content-box .suggestions-box');
	

	hide_btn.onclick = function () {
		content.classList.toggle('hidden');
		hide_btn.classList.toggle('hidden');
		box.classList.toggle('del-width');
		change_text_suggest();
	}




}

function change_text_suggest() {
	var x = document.querySelector('#wrapper #main #post-detail-blog .main-post .content-post .content-box .suggestions-box .title-suggest .hide-and-show .hide');
	if (x.innerHTML == '[hide]') {
		x.innerHTML = "[show]";
	}
	else {
		x.innerHTML = "[hide]";
    }


}

function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
		);
	});
}
